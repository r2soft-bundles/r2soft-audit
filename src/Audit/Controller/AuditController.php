<?php

namespace R2Soft\Audit\Controller;

use Hamcrest\Util;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Auth;
use R2Soft\Audit\Business\AuditBo;
use R2Soft\Audit\Message\Message;
use R2Soft\Audit\Utils\Utils;


class AuditController extends Controller
{

    private $responseFactory;

    private $auditBo;

    public function __construct(ResponseFactory $responseFactory, AuditBo $auditBo)
    {
        $this->responseFactory = $responseFactory;
        $this->auditBo = $auditBo;
    }

    public function index()
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        return $this->responseFactory->view('Audit::index');
    }

    public function getAudit(int $id, $idUpdate = null)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }
        $audits =  $this->auditBo->getAudit($id, $idUpdate);
        return $this->responseFactory->view('Audit::audit', compact('audits'));
    }

    public function all(Request $request)
    {
        if(!Auth::check ()){
            return redirect()->route('login');
        }
        return $this->auditBo->all($request->all());
    }

}