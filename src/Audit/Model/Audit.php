<?php

namespace R2Soft\Audit\Model;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{

    protected $table = 'auditoria';

    protected $fillable = ['tabela', 'hora','data','type','usuario','estacao','ip_estacao','ordem','campos'];

    public $timestamps = false;
}