<?php


namespace R2Soft\Audit\Business;

use R2Soft\Audit\Repository\AuditRepository;

class AuditBo
{

    private $auditRepository;

    public function __construct(AuditRepository $auditRepository)
    {
        $this->auditRepository = $auditRepository;
    }

    public function all(array $request)
    {
        return $this->auditRepository->all($request);
    }

    public function getAudit(int $id, $idUpdate = null)
    {
        return $this->auditRepository->getAudit($id, $idUpdate);
    }
}