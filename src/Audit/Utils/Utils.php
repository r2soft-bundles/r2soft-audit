<?php

namespace R2Soft\Audit\Utils;


abstract class Utils
{


    public static function getHstoreToArray(string $data) :array
    {
        $data = '{' . str_replace('"=>"', '":"', $data) . '}';
        $data =  str_replace('=>NULL', ':"NULL"', $data);
        return json_decode($data, true);
    }

}