<?php

namespace R2Soft\Audit\Repository;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use R2Soft\Audit\Model\Audit;
use R2Soft\Audit\Utils\Utils;

class AuditRepository
{

    private $audit;

    public function __construct(Audit $audit)
    {
        $this->audit = $audit;
    }

    public function getModel()
    {
        return $this->audit;
    }

    public function all(array $request)
    {
        $user = Auth::user();
        $draw = $request['draw'];
        $row = $request['start'];
        $rowperpage = $request['length']; // Rows display per page
        $columnIndex = $request['order'][0]['column']; // Column index
        $columnName = $request['columns'][$columnIndex]['data']; // Column name
        $searchValue = $request['search']['value']; // Search value

        $data  = $this->getModel()
            ->select("type", "data", "hora", "id", "tabela",'estacao', 'ordem','usuario',
                DB::raw("(select name from users where email = REPLACE(usuario, 'r2sas-', '')) as name"),
                DB::raw("
                (CASE 
                  WHEN type = 'U' THEN
                    campos->'id'
                END) AS id_update")

            )
            ->where(function ($query) use ($user){
                $query->where('estacao', $user->institution_id?$user->institution_id:'')
                    ->where('type', 'D');
            })
            ->orWhere(function($query) use ($user){
                $query->where('type', '!=', 'D')
                    ->where('estacao', $user->institution_id?$user->institution_id:'')
                    ->where('ordem', 'NOVO');
            });

        $totalRecords = $data->count();
        $totalRecordsFilter = $data->count();

        if($searchValue !== '') {
            $data
                ->where ( 'tabela' , 'ilike' , '%' . $searchValue . '%' )
                ->orWhere ( 'usuario' , 'ilike' , '%' . $searchValue . '%' )
                ->orWhere ( 'data' , 'ilike' , '%' . $searchValue . '%' )
                ->orWhere ( 'type' , 'ilike' , '%' . $searchValue . '%' );
            $totalRecordsFilter = $data->count();
        }
        $data = $data->limit($row)->orderBy($columnName)->limit($rowperpage)->offset($row);

        echo json_encode( [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordsFilter,
            "aaData" => $data->get()
        ]);
    }

    public function getAudit(int $id, $idUpdate)
    {
        $user = Auth::user();

        if(!empty($id)){
            $sqlId = " and id = {$id} ";
        }else{
            return false;
        }

        if($user->institution_id){
            $institution = $user->institution_id;
        }else{
            $institution = '';
        }

        $sqlTableUpdated = '';
        if($idUpdate != 'null'){
            $sqlId = '';
            $update = DB::select("select data, hora, tabela, campos       
              from auditoria  where campos->'id' = '{$idUpdate}' and id = $id and 
              type = 'U' and estacao = '{$institution}'");

            $sqlTableUpdated = " and tabela =  '{$update[0]->tabela}' and data = '{$update[0]->data}' AND 
            campos->'id' = '{$idUpdate}' AND hora = '{$update[0]->hora}' and type = 'U' ";
        }

        $datas = DB::select("
        select tabela, data, hora,(select name from users where email = REPLACE(usuario, 'r2sas-', '')) as name, ordem, campos,
            (CASE 
                  WHEN type = 'I' THEN
                    'Inserido'
                  
                  WHEN type = 'U' THEN  
                    'Atualizado'
                  
                  ELSE
                   'Deletado'
            END) as type
        
        from auditoria  where estacao = '{$institution}'  {$sqlId} $sqlTableUpdated");

        if(!empty($datas[0])){
            $audits = [];
            foreach($datas as $data) {
                $data->campos = Utils::getHstoreToArray($data->campos);
                $audits[] = $data;
            }
            return $audits;
        }
        return [];
    }

}