

---Adicionar no final classe DatabaseSeeder no metodo run()


\Illuminate\Support\Facades\DB::select("SELECT * FROM public.recria_triggers_log();");

\Illuminate\Support\Facades\DB::select('DROP TRIGGER trg_users_log ON public.users;');

\Illuminate\Support\Facades\DB::select('CREATE TRIGGER trg_users_log AFTER INSERT OR UPDATE OF
                                            id, name, email, "isAdmin", institution_id, email_verified_at, password, actived, path_image
                                             OR DELETE ON public.users
                                             FOR EACH ROW EXECUTE PROCEDURE public.fn_auditoria();');