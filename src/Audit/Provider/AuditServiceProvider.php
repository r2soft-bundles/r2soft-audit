<?php

namespace R2soft\Audit\Provider;

use Illuminate\Support\ServiceProvider;


class AuditServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([__DIR__."/../../resources/migrations/"=>base_path('database/migrations')], 'migrations');
        $this->publishes([__DIR__."/../../public/js/"=>base_path('public/js')], 'js');
        $this->publishes([__DIR__."/../../public/css/"=>base_path('public/css')], 'css');
        $this->loadViewsFrom(__DIR__.'/../../resources/views/audit', 'Audit');

        require __DIR__ . '/../routes.php';
    }

    public function register(){}

}