<?php

Route::group(["prefix" => "audits", "middleware"=>['web']], function (){

    Route::get('/', '\R2Soft\Audit\Controller\AuditController@index')->name('index.audit');
    Route::get('/all', '\R2Soft\Audit\Controller\AuditController@all')->name('all.audit');
    Route::get('/get/{id}/{idUpdate?}', '\R2Soft\Audit\Controller\AuditController@getAudit')->name('get-audit.audit');
});