@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{asset('css/datatable.css')}}">
@endsection

@section('content')

    <div class="content-heading">
        Auditoria
    </div>
    <div class="card">
        <div class="card-body" >
            <table class="table table-striped table-hover w-100 display" id="audit" >
                <thead>
                    <tr>
                        <th scope="col" width="1%">ID</th>
                        <th scope="col" width="10%">Usuário</th>
                        <th scope="col" width="10%">Tabela</th>
                        <th scope="col" width="10%">Data</th>
                        <th scope="col" width="10%">Tipo</th>
                        <th scope="col" width="2%">Ação</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@include('Audit::component.modal-lg', ['id'=>'modal', 'title'=>'Auditoria', 'size'=>'modal-xl'])
@endsection
@section('scripts')

    <script src="{{asset('js/pdfmake.js')}}"></script>
    <script src="{{asset('js/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/datatable.js')}}"></script>

    <script src="{{asset('js/dataTableAudit.js')}}"></script>

    <script>

        tableAudit('/audits/all');

        function dataAudit(id, id_update){
            let urlAudit = window.location.origin+'/audits/get/'+id+'/'+id_update;

            $.ajax({
                url:urlAudit,
                type:'get',
                success: (response)=>{
                    $('.modal-title').text("Auditoria");
                    $('.modal-body').html(response);
                }
            })
        }
    </script>
@endsection