
<table class="table table-striped table-hover w-100 display table-responsive">
    <thead>
    <tr>
        @foreach($audits[0]->campos as $key => $audit)
            <th scope="col">{{$key}}</th>
        @endforeach
            <th scope="col">Ordem</th>
            <th scope="col">Usuário</th>
    </tr>
    </thead>
    <tbody>
        @foreach($audits as $key => $audit)
            <tr>
                @foreach($audit->campos as $key => $campos)
                    <td>{{$campos}}</td>
                @endforeach
                    <td>{{$audit->ordem}}</td>
                    <td>{{$audit->name}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
