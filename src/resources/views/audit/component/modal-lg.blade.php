@section('body-area')
    @empty($size)
        @php($size='modal-lg')
    @endempty
    <div class="modal fade" id="{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSmall" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog {{$size}}" id="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabelSmall">{{$title}}</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="card-body whirl traditional" id="whirl"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
@endsection
