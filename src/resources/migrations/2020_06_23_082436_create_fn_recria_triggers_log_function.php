<?php

use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class CreateFnRecriaTriggersLogFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::select( "CREATE OR REPLACE FUNCTION public.recria_triggers_log()
            RETURNS integer AS
            \$body$
            DECLARE
                ri RECORD;
            BEGIN
                FOR ri in SELECT schemaname, tablename FROM pg_catalog.pg_tables WHERE schemaname NOT IN ('pg_catalog', 'information_schema', 'pg_toast') and tablename not in ('auditoria', 'accesses') ORDER BY schemaname, tablename
                    LOOP
                        Execute 'DROP TRIGGER IF EXISTS \"trg_'|| ri.tablename ||'_log\" ON \"'|| ri.schemaname ||'\".\"'|| ri.tablename ||'\";';
                    END LOOP;
                FOR ri in SELECT schemaname, tablename FROM pg_catalog.pg_tables WHERE schemaname NOT IN ('pg_catalog', 'information_schema', 'pg_toast') and tablename not in ('auditoria', 'accesses') ORDER BY schemaname, tablename
                    LOOP
                        Execute 'CREATE TRIGGER \"trg_'|| ri.tablename ||'_log\" AFTER INSERT OR UPDATE OR DELETE ON \"'|| ri.schemaname ||'\".\"'|| ri.tablename ||'\" FOR EACH ROW EXECUTE PROCEDURE \"fn_auditoria\"();';
                    END LOOP;
                RETURN 1;
            END;
            \$body$
            LANGUAGE 'plpgsql'
            VOLATILE
            CALLED ON NULL INPUT
            SECURITY INVOKER
            COST 100;");

        DB::select('
         ALTER FUNCTION public.recria_triggers_log ()
              OWNER TO postgres;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::select('DROP FUNCTION public.recria_triggers_log();');
    }
}
