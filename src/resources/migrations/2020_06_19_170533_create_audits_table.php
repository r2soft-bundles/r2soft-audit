<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::select('CREATE EXTENSION IF NOT EXISTS hstore;');

        \Illuminate\Support\Facades\DB::select("CREATE TABLE auditoria(
                id bigserial,
                tabela text,
                data date default current_date,
                hora time WITHOUT TIME ZONE default CURRENT_TIME,
                type char(1),
                usuario text,
                estacao text,
                ip_estacao text,
                ordem text,
                campos hstore
            );");
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditoria');
    }
}
