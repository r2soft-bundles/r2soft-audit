
function tableAudit(url) {

    let urlAudit = window.location.origin+url;
    var table = $('#audit').DataTable({
        'paging': true,
        'processing': true,
        'serverSide': true,
        'serverMethod': 'get',
        'info': true, // Bottom left status text
        "ajax": {'url':urlAudit},
        'columns': [
            {'data': 'id'},
            {'data': 'name'},
            {'data': 'tabela'},
            {
                'data': 'data', render: (data, type, row) => {
                let hour = row['hora'].split('.')[0];
                return data.substr(0, 10).split('-').reverse().join('/') + ' ' + hour;
            }
            },
            {
                'data': 'type', render: (data) => {
                switch (data) {
                    case "U":
                        return 'Atualizado';
                        break;

                    case "I":
                        return 'Inserido';
                        break;

                    case "D":
                        return 'Deletado';
                        break;
                }
            }
            },
            {'data': 'id'},
        ],
        "fnRowCallback": function (linha, data) {
            linha.cells[5].innerHTML = `
                    <a class="dropdown-item text-muted btn-edit text-center" style="color: white; cursor: pointer" 
                    data-id="${data['id']}" data-toggle="modal" data-target="#modal" onclick="dataAudit(${data['id']}, ${data['id_update']})">
                     <em class="mr-2  mr-2 fas fa-eye"></em> Ver</a>                
                `;

        },
        responsive: true,
        "language": {
            "sSearch": '<em class="fas fa-search"></em>',
            "sLengthMenu": '_MENU_ Resultados por página',
            "info": 'Exibindo página _PAGE_ de _PAGES_',
            "zeroRecords": 'Não encontrado - Desculpe!',
            "infoEmpty": 'Nenhum registro disponível',
            "infoFiltered": '(Filtrado do total de _MAX_ registros)',
            "oPaginate": {
                "sNext": '<em class="fa fa-caret-right"></em>',
                "sPrevious": '<em class="fa fa-caret-left"></em>'
            },
            'processing':'<span style="color: #ffffff; font-size: 20px; background-color: #0f6674; padding: 10px">Por favor aguarde...</span>'
        },
        // dom: 'Blfrtip',
        buttons: [
            {extend: 'copy', className: 'action-datatable'},
            {extend: 'csv', className: 'action-datatable'},
            {extend: 'excel', className: 'action-datatable', title: $('title').text()},
            {extend: 'pdf', className: 'action-datatable', title: $('title').text()},
            {extend: 'print', className: 'action-datatable'}
        ]

    });

    $(".dataTables_filter input")
        .unbind().bind("input", function(e) {

        if(this.value.length >= 3 || e.keyCode == 13) {
            table.search(this.value).draw();
        }
        if(this.value == "") {
            table.search("").draw();
        }
        return;
    });
}