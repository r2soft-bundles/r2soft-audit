<?php

namespace Tests;

use Orchestra\Testbench\TestCase;

abstract class AbstractTestCase extends TestCase
{
    /**
     * Setup the test environment.
     */
    protected function setUp():void
    {
        parent::setUp();
        $this->loadMigrationsFrom(__DIR__.'/../src/resources/migrations');
    }
    public function migrate()
    {
        $this->loadLaravelMigrations(['--database' => 'pgsql']);
        $this->artisan('migrate', ['--database' => 'pgsql'])->run();
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'pgsql');
        $app['config']->set('database.connections.pgsql', [
            'driver'   => 'pgsql',
            'host'=>'127.0.0.1',
            'port'=>'5432',
            'database'=>'app_r2sas',
            'username'=>'postgres',
            'password'=>'root',
            'schema'=>'public',
            'prefix'   => '',
        ]);
    }

}