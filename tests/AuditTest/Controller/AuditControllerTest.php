<?php

namespace Tests\AuditTest\Model;

use Illuminate\Routing\ResponseFactory;
use R2Soft\Audit\Business\AuditBo;
use R2Soft\Audit\Controller\AuditController;
use Tests\AbstractTestCase;
use Mockery as mock;

class AuditControllerTest extends AbstractTestCase
{

    public function setUp(): void
    {
        parent::setUp ();
        $this->migrate();
    }

    public function testStanceOfController()
    {
        $response = mock::mock(ResponseFactory::class);
        $bo = mock::mock(AuditBo::class);
        $controller = new AuditController($response, $bo);
        $this->assertInstanceOf(\R2Soft\Audit\Controller\Controller::class, $controller);
    }

    public function testIfReturnIndex()
    {
        $response = mock::mock(ResponseFactory::class);
        $bo = mock::mock(AuditBo::class);
        $html = '<h1>Teste</h1>';
        $controller = new AuditController($response, $bo);

        $response->shouldReceive('view')->with('Audit::index')->andReturn($html);

        $this->assertEquals($controller->index(), $html);
    }


}