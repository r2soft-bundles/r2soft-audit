<?php

namespace Tests\AuditTest\Model;

use R2Soft\Audit\Model\Audit;
use R2Soft\Audit\Repository\AuditRepository;
use R2Soft\Audit\Utils\Utils;
use Tests\AbstractTestCase;
use Mockery as mock;

class AuditRepositoryTest extends AbstractTestCase
{

    private $repository;

    public function setUp(): void
    {
        parent::setUp ();
        $this->migrate();
        $model = new Audit();
        $this->repository =  new AuditRepository($model);
    }

    public function testStanceOfModel()
    {
        $this->assertInstanceOf(Audit::class, $this->repository->getModel());
    }

    public function testIfSaveAudit()
    {
        $audit = $this->repository->getModel()->create(
            ['tabela'=>'table', 'hora'=>date('H:i:s'),'data'=>date('d/m/Y'),'type'=>'I','usuario'=>'r2soft',
                'estacao'=>'estacao','ip_estacao'=>'ip','ordem'=>'Criado','campos'=>'teste=>teste']
        );

        $this->assertEquals('I', $audit->type);
    }

    public function testGetByIdAudit()
    {
        $audit = $this->repository->getModel()->create(
            ['tabela'=>'table', 'hora'=>date('H:i:s'),'data'=>date('d/m/Y'),'type'=>'I','usuario'=>'r2soft',
                'estacao'=>'estacao','ip_estacao'=>'ip','ordem'=>'Criado','campos'=>'teste=>teste']
        );

        $getAudit = $this->repository->getModel()->find($audit->id);

        $this->assertEquals('r2soft', $getAudit->usuario);
    }

    public function testAllAudit()
    {
        $data = [
            'tabela'=>'table', 'hora'=>date('H:i:s'),'data'=>date('d/m/Y'),'type'=>'I','usuario'=>'r2soft',
            'estacao'=>'estacao','ip_estacao'=>'ip','ordem'=>'Criado','campos'=>'teste=>teste, family_id=>1'
        ];

        $this->repository->getModel()->create($data);
        $this->repository->getModel()->create($data);
        $audit = $this->repository->getModel()->create($data);

        $allAudit = $this->repository->getModel()->orderBy('id', 'DESC')->get();

        $this->assertEquals($audit->id, $allAudit[0]->id);
    }

    public function testSearchUser()
    {
        $audit = $this->repository->getModel()->where('usuario', 'r2soft')->first();
        $this->assertEquals('r2soft', $audit->usuario);
    }


}