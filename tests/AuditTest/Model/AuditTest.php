<?php

namespace Tests\AuditTest\Model;

use R2Soft\Audit\Model\Audit;
use Tests\AbstractTestCase;

class AuditTest extends AbstractTestCase
{

    private $model;

    public function setUp(): void
    {
        parent::setUp ();
        $this->migrate();
        $this->model =  new Audit();
    }

    public function testIfSaveAudit()
    {
        $audit = $this->model->create(
            ['tabela'=>'table', 'hora'=>date('H:i:s'),'data'=>date('d/m/Y'),'type'=>'I','usuario'=>'r2soft',
            'estacao'=>'estacao','ip_estacao'=>'ip','ordem'=>'Criado','campos'=>'teste=>teste']
        );

        $this->assertEquals('I', $audit->type);
    }

    public function testGetByIdAudit()
    {
        $audit = $this->model->create(
            ['tabela'=>'table', 'hora'=>date('H:i:s'),'data'=>date('d/m/Y'),'type'=>'I','usuario'=>'r2soft',
            'estacao'=>'estacao','ip_estacao'=>'ip','ordem'=>'Criado','campos'=>'teste=>teste']
        );

        $getAudit = $this->model->find($audit->id);

        $this->assertEquals('r2soft', $getAudit->usuario);
    }

    public function testAllAudit()
    {
        $data = [
            'tabela'=>'table', 'hora'=>date('H:i:s'),'data'=>date('d/m/Y'),'type'=>'I','usuario'=>'r2soft',
            'estacao'=>'estacao','ip_estacao'=>'ip','ordem'=>'Criado','campos'=>'teste=>teste'
        ];

        $this->model->create($data);
        $this->model->create($data);
        $audit = $this->model->create($data);

        $allAudit = $this->model->orderBy('id', 'DESC')->get();

        $this->assertEquals($audit->id, $allAudit[0]->id);
    }

    public function testSearchUser()
    {
        $audit = $this->model->where('usuario', 'r2soft')->first();
        $this->assertEquals('r2soft', $audit->usuario);
    }

}